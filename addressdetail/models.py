from django.db import models

class AddressDetail(models.Model):
    addressee = models.CharField(max_length=255)
    address1 = models.CharField(max_length=300)
    address2 = models.CharField(max_length=300, blank=True)
    subDistrict = models.CharField(max_length=160)
    district = models.CharField(max_length=300, blank=True)
    city = models.CharField(max_length=255, blank=True)
    province = models.CharField(max_length=255)
    postalCode = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    email = models.EmailField()
