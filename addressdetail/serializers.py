from rest_framework import serializers
from addressdetail.models import AddressDetail

class AddressDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressDetail
        fields = ('addressee', 'address1', 'address2', 'subDistrict', 'district', 'city','province','postalCode',
                 'country','phone','email')
