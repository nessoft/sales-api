from django.apps import AppConfig


class SalesorderConfig(AppConfig):
    name = 'salesorder'
