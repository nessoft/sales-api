from django.db import models
from django_postgres_extensions.models.fields import ArrayField

from addressdetail.models import AddressDetail
from orderitem.models import OrderItem

PAYMENT_TYPE = [("NON_COD","Non cash on delivery"), ("COD","Cash on delivery"), ("CCOD","Credit card on delivery")]
CURRENCY_TYPE = [("THB","Thai Baht"),("SGD","Singapore dollar"),("IDR","Indonesia rupiah"),("PHP","Philippine peso")]
SHIPPING_TYPE = [(shipping,"Name") for shipping in ("SAME_DAY","NEXT_DAY","EXPRESS_1_2_DAYS","STANDARD_2_4_DAYS","NATIONWIDE_3_5_DAYS")]

class SalesOrder(models.Model):
    orderCreatedTime = models.DateTimeField(auto_now_add=True)
    customerInfo = models.ForeignKey(AddressDetail, related_name='salesorder_customerInfo', on_delete=models.CASCADE,null=True)
    orderShipmentInfo = models.ForeignKey(AddressDetail, related_name='salesorder_orderShipmentInfo', on_delete=models.CASCADE,null=True)
    customerTaxId = models.CharField(max_length=128)
    customerTaxType = models.CharField(max_length=128, default="Non-register")
    customerBranchCode = models.CharField(max_length=128, default="Emporium")
    paymentType = models.CharField(choices=PAYMENT_TYPE, default='NON_COD', max_length=128)
    shippingType = models.CharField(choices=SHIPPING_TYPE, default='SAME_DAY', max_length=128)
    grossTotal = models.DecimalField(max_digits=14,decimal_places=2)
    currUnit = models.CharField(choices=CURRENCY_TYPE, default='THB', max_length=128)
    orderItems = models.ManyToManyField(OrderItem)
