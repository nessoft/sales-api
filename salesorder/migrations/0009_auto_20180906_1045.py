# Generated by Django 2.1.1 on 2018-09-06 03:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orderitem', '0002_auto_20180905_1806'),
        ('salesorder', '0008_salesorder_orderitems'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='salesorder',
            name='orderItems',
        ),
        migrations.AddField(
            model_name='salesorder',
            name='orderItems',
            field=models.ManyToManyField(to='orderitem.OrderItem'),
        ),
    ]
