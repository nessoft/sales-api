# Generated by Django 2.1.1 on 2018-09-06 04:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orderitem', '0002_auto_20180905_1806'),
        ('salesorder', '0009_auto_20180906_1045'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='salesorder',
            name='orderItems',
        ),
        migrations.AddField(
            model_name='salesorder',
            name='orderItems',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orderItem', to='orderitem.OrderItem'),
        ),
    ]
