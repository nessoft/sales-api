# Generated by Django 2.1.1 on 2018-09-05 09:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('salesorder', '0004_auto_20180905_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesorder',
            name='customerInfo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='salesorder_customerInfo', to='addressdetail.AddressDetail'),
        ),
        migrations.AlterField(
            model_name='salesorder',
            name='orderShipmentInfo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='salesorder_orderShipmentInfo', to='addressdetail.AddressDetail'),
        ),
    ]
