from rest_framework import serializers
from salesorder.models import SalesOrder, PAYMENT_TYPE, CURRENCY_TYPE

from addressdetail.serializers import AddressDetailSerializer
from orderitem.serializers import OrderItemSerializer

class SalesOrderSerializer(serializers.ModelSerializer):
    customerInfo = AddressDetailSerializer()
    orderShipmentInfo = AddressDetailSerializer()
    orderItems = OrderItemSerializer(read_only=True, many=True)
    class Meta:
        model = SalesOrder
        fields = ('customerInfo','orderShipmentInfo','orderCreatedTime', 'customerTaxId', 'customerTaxType', 'customerBranchCode', 'paymentType', 'shippingType','grossTotal','currUnit','orderItems')
