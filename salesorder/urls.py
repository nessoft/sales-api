from django.urls import path,include

from . import views

urlpatterns = [
    path('order/', views.salesOrder_list, name='allSalesOrder'),
    path('order/<int:orderID>', views.salesOrder_detail, name='salesOrderDetails')
]
