from salesorder.models import SalesOrder
from salesorder.serializers import SalesOrderSerializer
from rest_framework import generics

from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

@csrf_exempt
def salesOrder_list(request, channelID):
    if request.method == 'GET':
        saleOrders = SalesOrder.objects.all()
        serializer = SalesOrderSerializer(saleOrders, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SalesOrderSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def salesOrder_detail(request, channelID, orderID):

    try:
        #salesOrder = SalesOrder.objects.get(pk=orderID)
        salesOrder = get_object_or_404(SalesOrder,pk=orderID)
    except SalesOrder.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SalesOrderSerializer(salesOrder)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SalesOrderSerializer(salesOrder, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        salesOrder.delete()
        return HttpResponse(status=204)
