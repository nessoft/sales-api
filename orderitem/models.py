from django.db import models

class OrderItem(models.Model):
    qty = models.IntegerField(default=0)
    partnerId = models.CharField(max_length=255)
    itemId = models.CharField(max_length=255)
    subTotal = models.DecimalField(max_digits=14, decimal_places=2)
