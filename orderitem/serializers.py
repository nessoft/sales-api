from rest_framework import serializers
from orderitem.models import OrderItem

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('qty','partnerId','itemId','subTotal')
